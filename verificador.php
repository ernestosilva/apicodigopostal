<?php
	
    class verificador{

        var $servidor;
        var $username;
        var $password;
        var $bd;

        function __construct($servidor, $username, $password, $bd){
        	$this->servidor = $servidor;
        	$this->username = $username;
        	$this->password = $password;
        	$this->bd = $bd;
        }

        function getlocalidade($codigo, $extensao, $pais){
            $conexao = new mysqli($this->servidor, $this->username, $this->password, $this->bd);
    
            if ($conexao->connect_error) {
                die("Connection failed: " . $connect_error);
            }
    
            $query = "SELECT Localidade,Concelho,Distrito,DesignacaoP from CodigosP where Codigo LIKE '{$codigo}' AND Extensao LIKE '{$extensao}' AND Pais LIKE '{$pais}';";

            $result = $conexao->query($query);

            if ($result->num_rows === 1) {
                return $row = $result->fetch_assoc();
            } else {
                return 0;
            }
        }

        function verificaLocalidade($codigo, $extensao, $pais, $localidade){
            $conexao = new mysqli($this->servidor, $this->username, $this->password, $this->bd);
    
            if ($conexao->connect_error) {
                die("Connection failed: " . $connect_error);
            }
    
            $query = "SELECT Localidade,Concelho,Distrito,DesignacaoP from CodigosP where Codigo LIKE '{$codigo}' AND Extensao LIKE '{$extensao}' AND Pais LIKE '{$pais}' AND Localidade LIKE '{$localidade}';";
    
            $result = $conexao->query($query);

            if ($result->num_rows === 1) {
                return 1;
            } else {
                return 0;
            }
        }
    }
	
?>