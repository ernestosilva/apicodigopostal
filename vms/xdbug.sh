

PASSWORD = "$1"
PROJECTFOLDER="$2"
sudo sed -i "s/nameserver .*/nameserver 8.8.8.8/" /etc/resolv.conf

sudo mkdir "/var/www/html/xdebug_log"
sudo mkdir "/var/www/html/xdebug_log/logs"

sudo apt-get install update
sudo apt-get install -y php5-dev php-pear

#####################################
# IF NOT WORK INSTALL
#sudo wget http://pear.php.net/go-pear.phar
#sudo php go-pear.phar
#sudo service apache2 restart
#####################################
sudo pecl install xdebug

sudo sed -i '$ a\zend_extension="/usr/lib/php5/20121212/xdebug.so"' /etc/php5/apache2/php.ini
sudo sed -i '$ a\xdebug.profiler_enable_trigger=1' /etc/php5/apache2/php.ini
sudo sed -i '$ a\xdebug.profiler_output_dir="/var/www/html/xdebug_log/logs"' /etc/php5/apache2/php.ini
sudo sed -i '$ a\xdebug.profiler_output_name= "cachegrind.out.%u.%H.%R"' /etc/php5/apache2/php.ini

sudo service apache2 restart
