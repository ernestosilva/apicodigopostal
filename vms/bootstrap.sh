#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD = "$1"
PROJECTFOLDER="$2"

# create project folder
# sudo mkdir "/var/www/html/${PROJECTFOLDER}"

# update / upgrade
# sudo apt-get update
# sudo apt-get -y upgrade

# install apache 2.5 and php 5.5
sudo apt-get install -y apache2
sudo apt-get install -y php5

# install mysql and give password to installer
#sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
#sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server-5.5 mysql-server/root_password password bsolus"
sudo debconf-set-selections <<< "mysql-server-5.5 mysql-server/root_password_again password bsolus"
sudo apt-get -y install mysql-server
sudo apt-get -y install php5-mysql

# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
#sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
#sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
#sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
#sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
#sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
#sudo apt-get -y install phpmyadmin
sudo apt-get -y install php5-curl
### Install mongodb version 2.4.8
### http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/

# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
# echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
# sudo apt-get update
# sudo apt-get install -y mongodb-org

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/html"
    <Directory "/var/www/html">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

# enable mod_rewrite
sudo a2enmod rewrite
sudo a2enmod headers

# restart apache
sudo service apache2 restart

# install git
sudo apt-get -y install git
sudo apt-get -y install php5-mcrypt
sudo php5enmod mcrypt
sudo service apache2 restart

# install Composer
# curl -s https://getcomposer.org/installer | php
# mv composer.phar /usr/local/bin/composer

# sudo apt-get install -y memcached
# sudo apt-get install -y php5-memcached
#sudo service memcached start
# sudo service apache2 restart
#sudo apt-get install -y php-pear php5-dev make libpcre3-dev
# sudo apt-get -y install nginx
# sudo apt-get -y install php5-fpm
#install apache benchmark
# sudo apt-get -y install apache2-utils


#sudo apt-get -y install apt-transport-https
# sudo curl https://repo.varnish-cache.org/GPG-key.txt | apt-key add -
#sudo echo "deb https://repo.varnish-cache.org/ubuntu/ trusty varnish-4.1" >> /etc/apt/sources.list.d/varnish-cache.list
#sudo apt-get update
#sudo apt-get -y install varnish

sudo sed -i "s/post_max_size = .*/post_max_size = 100M/" /etc/php5/apache2/php.ini

sudo rm /var/www/html/index.html

sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL \& ~E_DEPRECATED \& ~E_STRICT \& ~E_NOTICE \& ~E_WARNING \& ~E_PARSE /" /etc/php5/apache2/php.ini
sudo sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php5/apache2/php.ini

sudo service apache2 restart



