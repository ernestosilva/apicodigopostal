sudo sed -i "s/display_errors = .*/display_errors = Off/" /etc/php5/apache2/php.ini
sudo sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php5/apache2/php.ini
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL \& ~E_DEPRECATED \& ~E_STRICT \& ~E_NOTICE \& ~E_WARNING/" /etc/php5/apache2/php.ini
sudo sed -i "s/display_startup_errors = .*/display_startup_errors = Off/" /etc/php5/apache2/php.ini
sudo sed -i "s/log_errors = .*/log_errors = On/" /etc/php5/apache2/php.ini
sudo sed -i "s/log_errors_max_len = .*/log_errors_max_len = 1024/" /etc/php5/apache2/php.ini
sudo sed -i "s/html_errors = .*/html_errors = On/" /etc/php5/apache2/php.ini
sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 100M/" /etc/php5/apache2/php.ini
sudo sed -i "s/post_max_size = .*/post_max_size = 100M/" /etc/php5/apache2/php.ini
sudo service apache2 restart

#sudo sed -i "s/display_errors = .*/display_errors = Off/" /etc/php5/fpm/php.ini
#sudo sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php5/fpm/php.ini
#sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL \& ~E_DEPRECATED \& ~E_STRICT \& ~E_NOTICE \& ~E_WARNING/" /etc/php5/fpm/php.ini
#sudo sed -i "s/display_startup_errors = .*/display_startup_errors = Off/" /etc/php5/fpm/php.ini
#sudo sed -i "s/log_errors = .*/log_errors = On/" /etc/php5/fpm/php.ini
#sudo sed -i "s/log_errors_max_len = .*/log_errors_max_len = 1024/" /etc/php5/fpm/php.ini
#sudo sed -i "s/html_errors = .*/html_errors = On/" /etc/php5/fpm/php.ini
#sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 100M/" /etc/php5/fpm/php.ini
#sudo sed -i "s/post_max_size = .*/post_max_size = 100M/" /etc/php5/fpm/php.ini
